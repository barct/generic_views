function get_form_submit(selector, btn){
	$(btn).prop('disabled', true);
	form = selector[0];
	fd = new FormData($(form)[0]);
	$.ajax({
	type: "POST",
	processData: false,
    contentType: false,
	url: form.action,
	data:  fd,
	async: true,
	beforeSend: ajaxBeforeSend,
	success:show_form_submit,
	});
}

function show_form_submit(result, msg, request){
	if (request.getResponseHeader("content-type") == "application/json; charset=utf-8"){
		if ($("#modal").attr("success")){
		success_callback = eval($("#modal").attr("success")) 
		if (success_callback) { //es una funcion?
		 	success_callback(result,msg,request); 
		}else{
		 	location.reload();
		}
	}else{
		location.reload();
	}
	$("#modal").modal("hide");
	$('#modal').data('bs.modal',false);
	}else{
		$("#modal .modal-content").html(result);
		$("#modal").modal("show"); 
	}
//	reload_links();
}


$(document).ajaxError(function( event, jqxhr, settings, exception ) {
 	if (jqxhr.status==403){
 		$("#modal .modal-content").html(jqxhr.responseText);
 	}else{
 		var layer = ""+jqxhr.responseText+"";
 		$("#modal .modal-content").html(layer);
 	}
});

function ajaxBeforeSend(xhr, settings) {
	xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	}

function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = $.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}


$.fn.GVPostInline = function(config) {
	$this=$(this)
	$this.GVPostInline = this
	$this.delegate("form","submit",this ,function(event){
		event.preventDefault();
		var $form = $(this);
		$.ajax({
		type: $form.attr("method"),
		url: $form.attr("action"),
		data: $form.serialize(),
		async: true,
		beforeSend: ajaxBeforeSend,
		success: function(result, msg, request){
			r = event.data.onSuccess(result, msg, request);
			if (r) {
				var dt = $form.attr("data-target"); 
				if (dt!=""){
					$(dt).html(result);
					}
				}
			}
		});	
	});
	if (config["onSuccess"]){
		this.onSuccess = config["onSuccess"];	
	}else{
		this.onSuccess = function(result, msg, request){
		}	
	}	
}
