$.fn.gv_select_treeview = function(tree_data) {
	if (this.attr("tree_data_load")!="true"){
		this.attr("tree_data_load","true")
		var input = this
		var div_selected_content=this.find('option:selected').text();
		if(!div_selected_content){
			div_selected_content = 'seleccione';
		}
		var div_selected=$('<div id="'+$(this).attr("id")+'_selected" class="btn btn-white">'+div_selected_content+'</div> ');
		var div_treeview=$('<div id="'+$(this).attr("id")+'_treeview"></div>');
		div_selected.click(function(){
			div_treeview.toggle();	
		});
		this.parent().append(div_selected);
		this.parent().append(div_treeview);
		var tv = $(div_treeview).treeview({
					data: tree_data,
					levels:1,
					onNodeSelected: function(event, node){
						$(div_selected).html(node.text);
						$(input).val(node.pk);
						$(div_treeview).hide();
						},
					onNodeUnselected: function(event, node){
						$(div_selected).html("seleccione");
						$(input).val("");
						},
					}).hide();
		pk = this.val();
		if (pk){
			var node = $(div_treeview).treeview('findNodesByValue', [pk,"pk"]);
			$(div_treeview).treeview('selectNode', node[0]).treeview('revealNode', node[0]);
		}
	}
	return this;
};

function config_filter_field_tree(field_name, tree_data){
	$(document).ready(
			function(){
				//$('#tree_{{ field.name }}').treeview({data: tree_data_{{ field.name }}});
				var $checkableTree = $('#filter_field_tree_' + field_name).treeview({
		          data: tree_data,
		          showIcon: false,
		          showCheckbox: true,
		          levels: 1,
		          onNodeChecked: function(event, node) {
		           	  checkableNodes = findCheckableNodess();
		           	  var checkbox='<label id="filter_field_tree_' + field_name+ '_label_filter_'+node.pk+'" class="middle"><input class="ace" type="checkbox" checked="checked" name="filter_' + field_name +'" value="'+node.pk+'"><span class="lbl">'+node.text+'</span></label>';
		              $('#filter_field_tree_' + field_name +'_form_checkbox').prepend(checkbox);
		            },
		           onNodeUnchecked: function (event, node) {
		              $('#filter_field_tree_' + field_name+ '_label_filter_'+node.pk).remove();
		            }
		        });
		        var findCheckableNodess = function() {
		        	if ($('#filter_field_tree_' + field_name+ '_input-check-node').val().length > 5){
		          		var results=$checkableTree.treeview('search', [ $('#filter_field_tree_' + field_name+ '_input-check-node').val(), { ignoreCase: true, exactMatch: false, revealResults:true } ]);
		          		var output = '<p>' + results.length + ' encontrados</p>';
          				$.each(results, function (index, result) {
            				output += '<p>- ' + result.text + '</p>';
          				});
          				$('#filter_field_tree_' + field_name+ '_search-output').html(output);
		          		return results;

		        	}else{
		        		return Array();
		        	}
		        };
		        var checkableNodes = findCheckableNodess();
		        $('#filter_field_tree_' + field_name+ '_input-check-node').on('keyup', function (e) {
		          checkableNodes = findCheckableNodess();
		          $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
		        });
		        $('#filter_field_tree_' + field_name+ '_btn-toggle-checked.check-node').on('click', function (e) {
		          $checkableTree.treeview('toggleNodeChecked', [ checkableNodes, { silent: false }]);
		        });
		        $('#filter_field_tree_' + field_name+ '_btn-uncheck-all').on('click', function (e) {
		          $checkableTree.treeview('uncheckAll', { silent: false });
		        });
			}
		);
}