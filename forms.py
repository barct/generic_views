from django import forms
import widgets


class ModelTreeField(forms.ModelChoiceField):
    def __init__(self, queryset, related_name, *args, **kwargs):
        collector = Collector()
        collector.collect(queryset, related_name)
        self.widget = widgets.TreeView(queryset, related_name)
        super(ModelTreeField, self).__init__(
            queryset=collector.get_queryset(queryset.model), *args, **kwargs)


class Collector(object):
    def __init__(self):
        pass

    def collect(self, queryset, related_name):
        for obj in queryset:
            self.add(obj)
            if hasattr(obj, related_name):
                qs = getattr(obj, related_name)
                self.collect(qs.all(), related_name)

    __objs = list()
    __ids = list()

    def add(self, obj):
        self.__objs.append(obj)
        self.__ids.append(obj.pk)

    @property
    def object_list(self):
        return self.__objs

    @property
    def id_list(self):
        return self.__ids

    def get_queryset(self, Model):
        return Model.objects.filter(pk__in=self.__ids)
