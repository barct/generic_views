# -*- coding: utf8 -*-


def oprint(obj, name="<no name>"):
    if obj is not None:
        title = "Dump: %s : %s" % (name,type(obj))
        print(title)
        title = str(obj)[:255]
        print(title)
        print ("-" * len(title))
        d = dir(obj)
        for f in d:
            try:
                print("%s : %s" % (str(f), getattr(obj, f)))
            except ValueError:
                print("%s : --Error--" % (str(f)))

        title = "Fin Dump: %s" % name
        print(title)
        print("-" * len(title))

    else:
        print("None")
