# -*- coding: utf-8 *-*
from django.db.models import ForeignKey
from django.db.models import Q
from django.db.models.fields.related import ForeignObjectRel, OneToOneField
from django.core.exceptions import FieldDoesNotExist

from .debug import oprint


class FieldInfo():
    def __init__(self, name, model_field):
        self.model_field = model_field
        self.name = name
        self.propertys = dict()

    @property
    def verbose_name(self):
        if hasattr(self.model_field, 'verbose_name'):
            return self.model_field.verbose_name.capitalize()
        else:
            return self.model_field.name.capitalize()

    def get_search_filter(self, word):
        try:
            word = str(word)
        except ValueError:
            pass

        if (hasattr(self.model_field, "get_internal_type") and
                "Integer" in self.model_field.get_internal_type()):
            try:
                word = int(word)
                return Q(**{self.name: word})
            except ValueError:
                return Q()

        return Q(**{self.name + "__icontains": word})

    def __eq__(self, other):
        if isinstance(other, FieldInfo):
            if other.name == self.name:
                return True
            else:
                return False
        elif isinstance(other, basestring):
            if other == self.name:
                return True
            else:
                return False
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_choices(self):
        return self.model_field.get_choices()


class ExtraFieldModel():
    verbose_name = ''
    field_object = None

    def __init__(self, fo):
        self.field_object = fo


class FieldsInfo():
    def __init__(self, objects):

        self.model = objects.model
        self.objects = objects
        self.all = dict()
        self.list_display = list()
        self.search_fields = list()
        self.filter_fields = list()

    def _get_or_create_field(self, field_name):

        field_names = field_name.split("__")
        field_object = None
        try:
            field_object = self.model._meta.get_field(field_names[0])
            #oprint(field_object)
            direct = field_object.concrete
            m2m = field_object.many_to_many
        except FieldDoesNotExist:
            #field_object = getattr(self.model, field_names[0])
            field_object = ExtraFieldModel(getattr(self.model, field_names[0]))
            # oprint(field_object)
            direct = None
            m2m = None
            field_object.verbose_name = field_name
        model = self.model

        # except:
        #    field_object, model, direct, m2m = (
        #        ExtraFieldModel(), None, None, None)
        #    field_object.verbose_name = field_name

        for i in range(1, len(field_names)):
            if (isinstance(field_object, ForeignObjectRel) or
                    isinstance(field_object, OneToOneField) or
                    isinstance(field_object, ForeignKey)):
                model = field_object.related_model
                field_object = model._meta.get_field(field_names[i])
                direct = field_object.concrete
                m2m = field_object.many_to_many
            else:

                raise Exception("Error in fieldname: %s" % field_name)

        if field_name not in self.all:
            fi = FieldInfo(field_name, field_object)
            fi.model = model
            fi.direct = direct
            fi.m2m = m2m
            if ((not m2m and direct and
                 isinstance(field_object, ForeignKey)) or
                    type(field_object) == ForeignKey):
                fi.hasChoices = True
            else:
                fi.hasChoices = False

            self.all[field_name] = fi
        else:
            fi = self.all[field_name]
        return fi

    def getField(self, field_name):
        return self.all[field_name]

    def setListDisplay(self, ld):
        for field_name in ld:
            fi = self._get_or_create_field(field_name)
            self.list_display.append(fi)

    def setSearchFields(self, sf):
        for field_name in sf:
            fi = self._get_or_create_field(field_name)
            self.search_fields.append(fi)

    def setFieldPropertys(self, fields_propertys):
        for field, prop in fields_propertys:
            fi = self._get_or_create_field(field)
            fi.propertys.update(prop)
            if 'verbose_name' in prop:
                fi.verbose_name = prop["verbose_name"].capitalize()
            if ('filter_field_type' in prop and
                    prop['filter_field_type'] == "tree"):
                self.configure_tree_view(fi)

    def setFilterFields(self, sf):
        for field_name in sf:
            fi = self._get_or_create_field(field_name)
            self.filter_fields.append(fi)

    def configure_tree_view(self, field):
        tree_model = field.model_field.related_model

        # buscar el campo parent
        for tree_field in tree_model._meta.get_fields():

            if tree_field.related_model == tree_model:
                # search for self reference
                if tree_field.related_name is None:
                    tree_field.related_name = tree_field.name + "_set"
                parent_field = str(tree_field.name) + "_id"
                tree_propertys = {
                    'model': tree_model,
                    'self_reference_field': tree_field.field,
                    'parent_field': lambda obj: getattr(obj, parent_field),
                    'related_name': tree_field.related_name,
                }

                if "field_display" in field.propertys:
                    tree_propertys["field_display"] = field.propertys[
                        "field_display"]
                else:
                    tree_propertys["field_display"] = "__str__"

                # |filter(**{tree_field.field.name + "__isnull": True})
                tree_model = tree_model.objects.all()
                tree_propertys["queryset"] = tree_model
                field.propertys["tree_propertys"] = tree_propertys

    def get_filter_fields_tree(self):
        trees = list()
        for field in self.filter_fields:
            if "tree_propertys" in field.propertys:
                trees.append(field)
        return trees
