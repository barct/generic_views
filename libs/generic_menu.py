# -*- coding: utf8 -*-
import re
import types
from django.urls import reverse


_var_index = 1


def _get_new_key():
    global _var_index
    key = "m%s" % _var_index
    _var_index += 1
    return key


class AbstractData(object):

    order = 0

    @property
    def childs(self):
        return self.__childs

    @childs.setter
    def childs(self, value):
        self.__childs = value

    def __init__(self, key=None):
        self.__childs = list()
        if key is None:
            key = _get_new_key()
        self.key = key

    def get_childs(self):
        return self.__childs

    def append_child(self, child):
        self.__childs.append(child)

    def search_by_key(self, key):
        for c in self.__childs:
            if hasattr(c, "key") and c.key == key:
                return c
        return None

    def has_childs(self):
        return len(self.childs) > 0


class RenderableMenu(AbstractData):
    template = ""
    template_li = ""
    template_ul = ""

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, value):
        if value:
            self._active = value
            if self._parent:
                self._parent.active = value

    def __init__(self, ad_obj):
        super(RenderableMenu, self).__init__(ad_obj.key)
        self.label = ad_obj.label
        self.icon = ad_obj.icon
        self.get_permissions = ad_obj.get_permissions
        self.group = ad_obj.group
        self.order = ad_obj.order
        self.get_url = ad_obj.get_url
        self.get_visible = ad_obj.get_visible
        self.color = ad_obj.color
        self.get_icon = ad_obj.get_icon

        self.show = True
        self.visible = True
        self._active = False
        self._parent = None

    def configure_permissions(self, context):
        return self.permissions

    def configure(self, context):
        request = context["request"]
        self.url = self.get_url()
        if self.url and re.search(self.url, r'^' + request.path):
            self.active = True

        self.permissions = self.get_permissions(context)

        self.visible = self.get_visible(context)

        self.icon = self.get_icon(context)

        for s in self.get_childs():
            s.configure(context)

    def submenu_iterator(self):
        _list = self.get_childs()
        for sm in _list:
            yield sm


class GenericMenu(AbstractData):

    renderer = RenderableMenu

    def __init__(self, label, key=None,
                 icon=None, order=0,
                 permissions=None, group=None, url=None, rurl=None):
        super(GenericMenu, self).__init__(key)
        self.label = label
        self.icon = icon
        self.permissions = permissions
        self.group = group
        self.order = order
        self.url = url
        self.rurl = rurl
        self.show = True
        self.visible = True
        self._active = False
        self.color = None

    def concrete_childs(self):
        childs = super(GenericMenu, self).get_childs()
        _list = list()
        for c in childs:
            if hasattr(c, "__call__"):
                c = c()
            if isinstance(c, dict):
                c = GenericMenu.convert_from_old_menu(c)
            if hasattr(c, "__iter__"):
                for sub_c in c:
                    _list.append(sub_c)
            else:
                _list.append(c)
        return _list

    def get_renderable_menu(self):
        obj = self.renderer(self)
        for sm in sorted(self.concrete_childs(), key=lambda x: x.order):
            sm_clone = sm.get_renderable_menu()
            sm_clone._parent = obj
            obj.append_child(sm_clone)
        return obj

    @staticmethod
    def convert_from_old_menu(old_menu):
        if hasattr(old_menu, "__call__"):
            return old_menu
        else:
            m = GenericMenu(
                label=old_menu["label"],
            )
            if "order" in old_menu:
                m.order = old_menu["order"]

            if "permissions" in old_menu:
                m.permissions = old_menu["permissions"]

            if "icon" in old_menu:
                m.icon = old_menu["icon"]

            if "url" in old_menu:
                m.url = old_menu["url"]

            if "menu" in old_menu:
                for sm in old_menu["menu"]:
                    m.append_child(GenericMenu.convert_from_old_menu(sm))
            return m

    def get_permissions(self, context=None):
        return self.permissions

    def get_icon(self, context=None):
        return self.icon

    def get_url(self, context=None):
        if self.url:
            return self.url
        elif self.rurl:
            if type(self.rurl) is tuple:
                url, args = self.rurl
                return reverse(url, args)
            else:
                return reverse(self.rurl)
        return None

    def get_visible(self, context):
        permissions = self.get_permissions(context)
        visible = True
        if (permissions and hasattr(context, "request")):
            request = context.request
            if isinstance(permissions, types.StringTypes):
                if permissions == "login":
                    visible = visible and request.user.is_authenticated()
                else:
                    visible = visible and request.user.has_perm(
                        permissions)
            else:
                visible = visible and request.user.has_perms(
                    permissions)
        return visible
