from .libs.generic_menu import RenderableMenu, GenericMenu


class LeftRenderableMenu(RenderableMenu):
    template = "generic_views/menu/left_menu.html"
    template_li = "generic_views/menu/left_menu_li.html"
    template_ul = "generic_views/menu/left_menu_ul.html"


class LeftMenu(GenericMenu):

    renderer = LeftRenderableMenu

    def append_child(self, child, parent_key=None):
        if parent_key is not None:
            parent = self.search_by_key(parent_key)
            if parent is not None:
                parent.append_child(child)
            else:
                raise IndexError("parent_key not found")
        else:
            super(LeftMenu, self).append_child(child)


class MenuManager():
    left_menu = LeftMenu("left-menu", "left_menu")


def register_left_menu(menu, parent_key=None):
    MenuManager.left_menu.append_child(menu, parent_key)
