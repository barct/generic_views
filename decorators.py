from django.shortcuts import render


class ExhibibleError(Exception):
    """Exception raised for show in http response.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """
    popup_template = 'generic_views/error_popup.html'
    plain_template = 'generic_views/error_plain.html'

    def __init__(self, request, type, title, description):
        self.type = type
        self.title = title
        self.description = description
        self.request = request

    def render(self, request):
        context = {
            'title': self.title,
            'description': self.description
        }
        if self.type.lower() == 'popup':
            t = self.popup_template
        else:
            t = self.plain_template
        return render(request, t, context)


def render_exhibible_errors(func):
    def func_wrapper(request, *args, **kwargs):
        try:
            r = func(request=request, *args, **kwargs)
        except ExhibibleError as ce:
            r = ce.render(request)
        return r
    return func_wrapper
