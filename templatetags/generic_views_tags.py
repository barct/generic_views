# -*- coding: utf8 -*-
from django.template import Library
from django import template
from urllib.parse import urlencode
from django.template import Context, loader
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.utils.safestring import mark_safe
import json
# from generic_views.libs.debug import oprint
from django.utils.formats import number_format
from django.urls import reverse, resolve
import re
from sre_compile import error
from generic_views.views import MixinView

register = Library()



def encoded_dict(in_dict):
    out_dict = {}
    for k, v in in_dict.items():
        if isinstance(v, str):
            v = v.encode('utf8');
        out_dict[k] = v
    return out_dict


@register.simple_tag(takes_context=True)
def render_inline_menu(context, list_view, obj):
    if list_view.get_inline_menu:
        menu = list_view.get_inline_menu(obj)
        t = loader.get_template('generic_views/inline_menu.html')
        c = {'menu': menu, }
        return t.render(c)
    else:
        return ""


@register.simple_tag(takes_context=True)
def render_link(context, view, obj=None):
    kwargs = context["kwargs"].copy()
    if isinstance(view, MixinView):
        if obj:
            kwargs[view.pk_var] = obj.pk
        t = loader.get_template(view.list_url_template)
        namespace = resolve(context.request.path).namespace
        try:
            url = reverse(namespace + ":" + view.view_name,
                          kwargs=kwargs)
        except error:
            raise Exception(
                "Duplicate PK varname: change pk_var in the view class")
    else:
        if obj:
            kwargs[view['pk_var']] = obj.pk
        url = reverse(view['viewname'], kwargs=kwargs)
        t = loader.get_template(view['url_template'])
    context['url'] = url
    context['view'] = view
    return t.render(context.flatten())


@register.simple_tag(takes_context=True)
def render_url_params(context, new_vars="", as_=None):
    keep_url = context['keep_url'].copy()
    if new_vars != "":
        t = template.Template(new_vars)
        new_vars = t.render(context)
        variables = new_vars.split("&")

        for v in variables:
            key, val = v.split("=")
            keep_url[key] = val
    if as_ == "as_hidden_input":
        t = template.Template(
            "{% for k,v in keep_url.items %}"
            "<input type='hidden' name='{{k}}' value='{{v}}'>"
            "{%endfor%}")
        return t.render(Context({'keep_url': keep_url}))
    return urlencode(encoded_dict(keep_url))


@register.simple_tag(takes_context=True)
def render_filter_field(context, field):
    if "tree_propertys" in field.propertys:
        t = loader.get_template('generic_views/filter_fields_tree.html')
        context = {'field': field, 'tree': field.propertys["tree_propertys"]}
    else:
        choices = field.choices
        t = loader.get_template('generic_views/filter_fields.html')
        context = {'field': field, 'choices': choices}
    return t.render(context)


class _WidgetContextWrapper(object):
    def __init__(self, widget=None, context=None):
        self.widget = widget
        self.context = context

    def __getattr__(self, attr):
        return getattr(self.widget, attr)

    def render(self, name, value, attrs=None):
        try:
            return self.widget.render(
                name, value, attrs=attrs, context=self.context)
        except TypeError:
            return self.widget.render(name, value, attrs=attrs)


@register.simple_tag(takes_context=True)
def contextfield(context, field):
    return field.as_widget(
        widget=_WidgetContextWrapper(field.field.widget, context))


def getFieldValue(obj, field_name):
    field_names = field_name.split("__")
    for f in field_names:
        if hasattr(obj, f) or hasattr(obj.__class__, f):
            obj = getattr(obj, f)
        else:
            return None
    return obj


class FieldNode(template.Node):
    def __init__(self, obj, field):
        self.obj = obj
        self.field = field

    def render(self, context):
        obj = context[self.obj]
        field = context[self.field]

        # resolve picklists/choices, with get_xyz_display() function
        # value = None
        get_choice = 'get_' + field.name + '_display'

        if hasattr(obj, get_choice):

            value = getattr(obj, get_choice)()
        else:
            value = getFieldValue(obj, field.name)
            #  else:
            #  value = None

        if value is None or value == "None":
            value = ""

        use_template = False

        if hasattr(field, 'propertys'):
            if 'template_name' in field.propertys:
                use_template = True
                template_name = field.propertys['template_name']
                t = template.loader.get_template(template_name)
            elif 'template' in field.propertys:
                use_template = True
                t = template.Template(field.propertys['template'])

        
        if use_template:
            context = {
                'value': value,
                'object': obj
            }
            return t.render(context)
        else:
            return str(value)


@register.filter
def getFieldVal(object, field):
    return getattr(object, field)


@register.tag()
def render_field(parser, token):

    try:
        # split_contents() knows not to split quoted strings.
        tag_name, obj, field = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires 2 argument" % token.contents.split()[0])
    return FieldNode(obj, field)


@register.simple_tag(takes_context=True)
def render_tree_json(context, tree_node):
    if tree_node:
        t = loader.get_template('generic_views/filter_fields_tree.json')
        return t.render({'tree_node': tree_node, })
    else:
        return ""


def jsonify(object):
    if isinstance(object, QuerySet):
        return mark_safe(serialize('json', object))
    return mark_safe(json.dumps(object))


register.filter('jsonify', jsonify)
jsonify.is_safe = True


@register.filter_function
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)


@register.filter(name='percentage')
def percentage(fraction, population):
    """
    percentage pone puntos en los miles y comma más 2 decimales
    TODO: Hacerlo multilenguaje
    """
    try:
        value = ((float(fraction) / float(population)) * 100)
    except ValueError:
        return ""

    value = number_format(value, 2, force_grouping=True)
    output = re.sub(r'(,)', '.', value)
    groups = output.split(".")
    decimal_part = groups.pop()
    int_part = ".".join(groups)
    return int_part + "," + decimal_part


@register.filter(name='fraction')
def fraction(fraction, population):
    """
    percentage pone puntos en los miles y comma más 2 decimales
    TODO: Hacerlo multilenguaje
    """
    try:
        value = ((float(fraction) / float(population)) * 100)
    except ValueError:
        return ""

    value = number_format(value, 4, force_grouping=True)
    output = re.sub(r'(,)', '.', value)
    groups = output.split(".")
    decimal_part = groups.pop()
    int_part = ".".join(groups)
    return int_part + "." + decimal_part
