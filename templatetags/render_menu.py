# -*- coding: utf8 -*-
from django.template import Library
import re
from generic_views.manager import get_menu
import types
from django.template import loader
from generic_views.menus import MenuManager

register = Library()


@register.simple_tag(takes_context=True)
def render_menu(context, menu_id):
    menu = getattr(MenuManager, menu_id)
    r_menu = menu.get_renderable_menu()
    r_menu.configure(context)
    template = loader.get_template(r_menu.template)
    context = {'menu': r_menu}
    return template.render(context)
