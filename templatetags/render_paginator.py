# -*- coding: utf8 -*-
from django.template import Library
register = Library()


def render_paginator(context, first_last_amount=2, before_after_amount=4):
    objects_page = context['objects_page']
    paginator = context['paginator']
    page_numbers = []

    # Pages before current page
    if objects_page.number > first_last_amount + before_after_amount:
        for i in range(1, first_last_amount + 1):
            page_numbers.append(i)

        page_numbers.append(None)

        for i in range(objects_page.number - before_after_amount,
                       objects_page.number):
            page_numbers.append(i)
    else:
        for i in range(1, objects_page.number):
            page_numbers.append(i)
    # Current page and pages after current page
    if (objects_page.number +
        first_last_amount +
            before_after_amount < paginator.num_pages):
        for i in range(objects_page.number,
                       objects_page.number + before_after_amount + 1):
            page_numbers.append(i)
        page_numbers.append(None)
        for i in range(paginator.num_pages - first_last_amount + 1,
                       paginator.num_pages + 1):
            page_numbers.append(i)
    else:
        for i in range(objects_page.number, paginator.num_pages + 1):
            page_numbers.append(i)
    return {
        'paginator': paginator,
        'objects_page': objects_page,
        'page_numbers': page_numbers,
        'keep_url': context['keep_url'],
        'page_sizes': paginator.page_sizes,
    }


register.inclusion_tag('generic_views/pagination.html',
                       takes_context=True)(render_paginator)
