# -*- coding: utf8 -*-
from generic_views.menus import GenericMenu, MenuManager
import logging
import inspect

logging.basicConfig(format='[%(filename)s:%(lineno)d] %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Module(GenericMenu):

    def __init__(self, name, label,
                 icon=None, order=None,
                 permissions=None, group=None):
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        logger.warning('Module is deprecated use MenuManager.left_menu in: %s Line: %s' % (
            filename, line_number))
        super(Module, self).__init__(
            label, name, icon=icon, order=order, permissions=permissions)
        self.key = name

    @property
    def name(self):
        return self.key

    def update(self, module):
        self.key = module.key
        self.label = module.label
        self.icon = module.icon
        self.order = module.order
        self.permissions = module.permissions
        self.submenu = self.submenu + module.submenu
        self.group = module.group

    def register_menu(self, menu):
        if hasattr(menu, "__call__"):
            m = menu
        else:
            m = Module(
                label=menu["label"],
                name=menu["label"],
            )
            if "order" in menu:
                m.order = menu["order"]

            if "permissions" in menu:
                m.permissions = menu["permissions"]

            if "icon" in menu:
                m.icon = menu["icon"]

            if "url" in menu:
                m.url = menu["url"]

            if "menu" in menu:
                menu.register_menu(menu["menu"])

        self.append_child(m)

    def register_submenu(self, submenu):
        self.submenu = submenu

    @property
    def menu(self):
        return {
            'label': self.label,
            'icon': self.icon,
            'order': self.order,
            'permissions': self.permissions,
            'menu': self.submenu
        }


class RegisterManager():
    def __init__(self):
        # self.menu = dict()
        # (filename, line_number,
        #  function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        # logger.warning('RegisterManager is deprecated use MenuManager.left_menu in: %s Line: %s' % (
        #     filename, line_number))
        self.modules = MenuManager.left_menu
        # self._temp_modules = dict()

    def register_module(self, module):
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        logger.warning('register_module is deprecated use left_menu in: %s Line: %s' % (
            filename, line_number))
        m = self.modules.search_by_key(module.key)
        if m:
            m.update(module)
            return m
        else:
            self.modules.append_child(module)
        return module

    def register_menu(self, module_name, menu):
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        logger.warning('register_menu is deprecated use left_menu in: %s Line: %s' % (
            filename, line_number))
        module = self.modules.search_by_key(module_name)
        if not module:
            module = GenericMenu(
                label=module_name.capitalize(),
                key=module_name)
            MenuManager.left_menu.append_child(module)
        module.append_child(GenericMenu.convert_from_old_menu(menu))

    def register_submenu(self, module_name, submenu):
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        logger.warning('register_submenu is deprecated use left_menu in: %s Line: %s' % (
            filename, line_number))
        m = self.modules.search_by_key(module_name)
        if m:
            module = m
        else:
            module = self.register_module(
                Module(module_name, module_name.capitalize()))
        module.register_submenu(submenu)

    def get_menu(self):
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
        logger.warning('get_menu is deprecated use left_menu in: %s Line: %s' % (
            filename, line_number))
        return MenuManager.left_menu


register_manager = RegisterManager()


def register_menu(view_class):
    (filename, line_number,
     function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
    logger.warning('register_menu is deprecated use left_menu in: %s Line: %s' % (
        filename, line_number))
    register_manager.register_menu(view_class.module_name, view_class.menu)


def register_module(name, label, icon=None,
                    order=None, permissions=None, group=None):
    (filename, line_number,
     function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
    logger.warning('register_module is deprecated use left_menu in: %s Line: %s' % (
        filename, line_number))

    module = MenuManager.left_menu.search_by_key(name)
    if not module:
        module = GenericMenu(
            label=label,
            key=name,
            icon=icon,
            order=order,
            permissions=permissions,
            group=group
        )
        MenuManager.left_menu.append_child(module)
    return module


def get_menu():
    (filename, line_number,
     function_name, lines, index) = inspect.getframeinfo(inspect.currentframe().f_back)
    logger.warning('get_menu is deprecated use left_menu in: %s Line: %s' % (
        filename, line_number))
    return MenuManager.left_menu
