# -*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe
from generic_views.libs.debug import oprint
from django.template import Context, loader, Template
import datetime
import json
from django.core.cache import cache
import zlib
from django.utils.encoding import smart_str


class ColorPicker(forms.widgets.Select):

    colors = (  "#ac725e", "#d06b64", "#f83a22", "#fa573c", "#ff7537", "#ffad46", "#42d692", "#16a765", "#7bd148",
               "#b3dc6c", "#fbe983", "#fad165", "#92e1c0", "#9fe1e7", "#9fc6e7", "#4986e7", "#9a9cff", "#b99aff",
               "#c2c2c2", "#cabdbf", "#cca6ac", "#f691b2", "#cd74e6", "#a47ae2", "#555",)

    def __init__(self, attrs=None, choices=list()):
        if attrs is None:
            attrs = dict()
        attrs["gv_widget"] = "color_picker"
        super(ColorPicker, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        for color in self.colors:
            choices.append((color, color))
        self.choices = list(choices)


#CACHE = get_cache('default')
class TreeView(forms.widgets.Select):
    def __init__(self, queryset=None, key=None, parent_field=None, selecteable_field=None, unique_js_var=None, attrs=None, *args, **kwargs):
        super(TreeView, self).__init__(attrs, *args, **kwargs)

        self.queryset = queryset
        self.parent_field = parent_field
        self.key = key
        self.selecteable_field = selecteable_field
        self.unique_js_var = unique_js_var

    def __build_tree_data(self):
        """Contruye un arbol con los datos del queryset"""
        node_list = list()

        if isinstance(self.parent_field, str):
            def parent_field(obj): return getattr(obj, self.parent_field)
        else:
            parent_field = self.parent_field

        if isinstance(self.selecteable_field, str):
            def selecteable_field(obj): return getattr(
                obj, self.selecteable_field)
        else:
            selecteable_field = self.selecteable_field

        if self.key is None:
            self.key = "pk"
        if isinstance(self.key, str):
            def key(obj): return getattr(obj, self.key)
        else:
            key = self.key

        for obj in self.queryset:
            node = {
                "id": key(obj),
                "parent": parent_field(obj),
                "obj": obj,
                "verbose_name": (obj),
            }
            if not selecteable_field is None:
                node["selectable"] = selecteable_field(obj)
            else:
                node["selectable"] = True
            node_list.append(node)

        arbol = arbolize(node_list,
                         key=lambda node: node["id"],
                         parent_key=lambda node: node["parent"],
                         )

        # TODO: Hacer que sea mas eficiente esto, ya que calcula lo mismo todo una y otra vez
        t = loader.get_template('generic_views/filter_fields_tree.json')
        tree_data = t.render({'tree_node': arbol, })
        return tree_data

    def __get_perivus_tree_data(self, var_key, context):
        """ si la variable var_key exuiste en el context, el arbol ya estaba construido, lo devuelve"""
        if not var_key is None and not context is None:
            if var_key in context.dicts[0]:
                return context.dicts[0][var_key]
            else:
                return None
        else:
            return None

    def __set_previus_tree_data(self, var_key, context, tree_data):
        """ pone en el context los datos del arbol """
        if not var_key is None and not context is None:
            context.dicts[0].update({var_key: tree_data})

    def render(self, name, value, context=None, attrs=None, *args, **kwargs):
        """renderiza el widget"""
        # buscamos si este arbol ya ha sido construido antes
        tree_data = self.__get_perivus_tree_data(self.unique_js_var, context)
        tree_var = self.unique_js_var

        # si no fue construido lo contruimos y los guardamos en el contxt
        if not tree_data:
            tree_data = self.__build_tree_data()
            if self.unique_js_var:
                tree_var = self.unique_js_var
            else:
                crc = zlib.crc32(smart_str(tree_data))
                tree_var = ("tree_" + (abs(crc)) + "_key")

            self.__set_previus_tree_data(tree_var, context, tree_data)
            define_tree = tree_var + "=" + tree_data + ";"
        else:
            define_tree = ""

        t = loader.get_template('generic_views/widgets/select_tree_view.html')
        script = t.render({"define_tree": define_tree, 'name': name,
                           'value': value, 'attrs': attrs, 'tree_var': tree_var})
        return script


# Toma una lista de dict y retorna las ramas padres y los hijos en
# la key childrens
# tree=(
#   {"id": 1, "parent": 0, "node_info": None},
#   {"id": 2, "parent": 0, "node_info": None},
#   {"id": 3, "parent": 1, "node_info": None},
#   {"id": 4, "parent": 1, "node_info": None},
#   {"id": 5, "parent": 1, "node_info": None},
#   {"id": 6, "parent": 10, "node_info": None},
#   {"id": 7, "parent": 10, "node_info": None},
#   {"id": 8, "parent": 1, "node_info": None},
#   {"id": 9, "parent":  6, "node_info": None},
#   {"id": 10, "parent": 2, "node_info": None},
#   {"id": 11, "parent": 9, "node_info": None},
#   {"id": 12, "parent": 1, "node_info": None},
#   )

# ar = arbolize(tree,
#   key=lambda node: node["id"] ,
#   parent_key = lambda node: node["parent"],
#   )
# print json.dumps(ar)
def arbolize(node_list, key, parent_key):
    is_child_list = list()
    node_list = sorted(node_list, key=parent_key)
    for p_node in node_list:
        node_iter = iter(node_list)
        for c_node in node_iter:
            if parent_key(c_node) == key(p_node):
                p_node["childrens"] = list()
                while c_node and parent_key(c_node) == key(p_node):
                    p_node["childrens"].append(c_node)
                    is_child_list.append(key(c_node))
                    try:
                        c_node = next(node_iter)
                    except StopIteration:
                        c_node = False
                break
    return [node for node in node_list if key(node) not in is_child_list]


class DatePicker(forms.widgets.TextInput):

    def __init__(self, attrs=None):
        if attrs is None:
            attrs = dict()
        attrs["gv_widget"] = 'date_picker'
        attrs["style"] = 'width:85px;'
        attrs["class"] = 'class="form-control date-picker"'
        super(DatePicker, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if isinstance(value, datetime.date):
            value = value.strftime("%d/%m/%Y")

        pre = '<div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar bigger-110"></i></span>'
        post = '</div>'

        return pre + super(DatePicker, self).render(name, value, attrs) + post
