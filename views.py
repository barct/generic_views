# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.views.generic import View
from django.http import Http404
from django.core.paginator import Paginator
from urllib.parse import urlencode
from django.db.models import Q
from .libs.field_info import FieldsInfo
from django.views.generic.edit import (
    BaseCreateView, BaseUpdateView, BaseDeleteView)
from django.conf.urls import include
from django.urls import re_path as url
from django.utils.decorators import classonlymethod
# from .libs.debug import oprint
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.core import serializers
from django.forms.fields import DateField
from django.contrib.admin.utils import NestedObjects
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, render
from django.urls import reverse
from . import widgets
import types
from .menus import MenuManager


class MixinView(SingleObjectTemplateResponseMixin):
    template_name = 'generic_views/form_view.html'
    popup = True
    pk_var = "pk"

    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        if isinstance(self, DeleteView):
            # Comportamiento específico para DeleteView
            self.object.delete()  # Elimina el objeto
        else:
            self.object = form.save()
        content = serializers.serialize("json", (self.object,))
        response = HttpResponse(content_type='application/json; charset=utf-8',
                                content=content)
        return response

    def get_initial(self):
        return self.request.GET.dict()

    def get_context_data(self, **kwargs):
        context = super(MixinView, self).get_context_data(**kwargs)
        if hasattr(self, 'model') and hasattr(self.model, '_meta'):
            context['verbose_name'] = (
                self.model._meta.verbose_name.capitalize())
            context['model_name'] = self.model.__name__
        return context

    def get_form(self, *args, **kwargs):
        form = super(MixinView, self).get_form(*args, **kwargs)
        return form

    def get_form_class(self):
        # configuro list_display
        if not self.form_class:
            if self.fields is None:
                self.fields = list()
                for item in self.model._meta.fields:
                    self.fields.append(item.name)

            form_class = super(MixinView, self).get_form_class()
            self.fields_info = FieldsInfo(form_class._meta)
            if hasattr(self, "fields_propertys"):
                self.fields_info.setFieldPropertys(self.fields_propertys)

            for field_name in form_class.base_fields:
                field = form_class.base_fields[field_name]
                if type(field) == DateField:
                    field.widget = widgets.DatePicker()

            for field_name in self.fields_info.all:
                fi = self.fields_info.all[field_name]
                for prop_name, prop in fi.propertys.iteritems():
                    if prop_name == "widget":
                        fp = form_class.base_fields[field_name]
                        if hasattr(prop, '__call__'):
                            fp.widget = prop()
                        else:
                            fp.widget = prop
                    elif prop_name == "filter_field_type" and prop == "tree":
                        fp = form_class.base_fields[field_name]
                        fp.widget = widgets.TreeView(
                            queryset=(
                                fi.propertys["tree_propertys"]["queryset"]),
                            parent_field=(
                                fi.propertys["tree_propertys"]["parent_field"]))
        else:
            form_class = self.form_class
        return form_class

    def dispatch(self, request, *args, **kwargs):
        if (hasattr(self, "login_required") and
                self.login_required is True):
            if not request.user.is_authenticated:
                return handler403(request)

        if hasattr(self, "permissions"):
            if isinstance(self.permissions, types.StringTypes):
                if self.permissions == "login":
                    if not request.user.is_authenticated:
                        return handler403(request)
                elif not request.user.has_perm(self.permissions):
                    return handler403(request)
            else:
                if not request.user.has_perms(self.permissions):
                    return handler403(request)

        return super(MixinView, self).dispatch(request, *args, **kwargs)

    @classonlymethod
    def as_view(cls, **initkwargs):
        return super(MixinView, cls).as_view(**initkwargs)


class CreateView(MixinView, BaseCreateView):

    list_url_template = "generic_views/urls/create.html"
    view_name = "create"

    @classonlymethod
    def as_view(cls, **initkwargs):
        urls = [
            url(r'^add/$', super(CreateView, cls).as_view(**initkwargs),
                name=cls.view_name),
        ]
        return include(urls)


class UpdateView(MixinView, BaseUpdateView):

    list_url_template = "generic_views/urls/update.html"
    view_name = "update"

    @classonlymethod
    def as_view(cls, **initkwargs):
        urls = [
            url(r'^update/(?P<%s>[^\\]+)/$' % cls.pk_var,
                super(UpdateView, cls
                      ).as_view(**initkwargs), name=cls.view_name),
        ]

        return include(urls)


class DeleteView(MixinView, BaseDeleteView):

    list_url_template = "generic_views/urls/delete.html"
    template_name = 'generic_views/confirm_delete_view.html'
    view_name = "delete"

    def get_context_data(self, **kwargs):
        context = super(DeleteView, self).get_context_data(**kwargs)
        obj = context["object"]
        collector = NestedObjects(using='default')
        collector.collect([obj])
        context["to_delete"] = collector.nested()
        context["protected"] = [o for o in collector.protected]
        return context

    def delete(self, request, *args, **kwargs):
        self.deleted_object = self.get_object()

        self.object = self.get_object()
        self.object.delete()

        content = serializers.serialize("json", (self.object,))
        response = HttpResponse(
            content_type='application/json; charset=utf-8',
            content=content)
        return response

    @classonlymethod
    def as_view(cls, **initkwargs):
        urls = [
            url(r'^delete/(?P<%s>\w+)/$' % cls.pk_var,
                super(DeleteView, cls).as_view(
                    **initkwargs), name=cls.view_name),
        ]

        return include(urls)


class ListView(View):

    template_name = 'generic_views/list_view.html'
    template_ods_name = 'generic_views/list_view_ods.html'
    template_print_name = 'generic_views/list_view_print.html'
    table_list_template_name = 'generic_views/table_list.html'
    list_display = None
    fields_propertys = None
    filter_fields = None
    search_fields = None
    page_size = 20
    page_sizes = (10, 20, 50, 100)
    export = False
    view_name = "list"

    menu_place = MenuManager.left_menu

    _default_url_templates = {
        "view": "generic_views/urls/view.html",  # not implemented yet
        "create": "generic_views/urls/create.html",
        "update": "generic_views/urls/update.html",
        "delete": "generic_views/urls/delete.html", }

    def __init__(self):
        self.keep_vars = dict()
        self.search = None
        self.order_by = None
        self.order_type = None
        self.filter = None
        self.add_filters = ""

        if hasattr(self, "url_templates"):
            utpl = self.url_templates.copy()
            self.url_templates = self._default_url_templates
            self.url_templates.update(utpl)
        else:
            self.url_templates = self._default_url_templates

    @classonlymethod
    def as_crud(cls, mixin_view, namespace=None, **initkwargs):
        url_list = url(r'^list/$',
                       super(ListView, cls).as_view(**initkwargs),
                       name=cls.view_name)
        urls = [
            url_list,
        ]

        class CV(CreateView, mixin_view):
            if (hasattr(mixin_view, "permissions") and
                    mixin_view.permissions == "default"):
                mixin_view.permissions_create = (
                    mixin_view.model._meta.app_label + ".add_" +
                    mixin_view.model._meta.model_name)
            if hasattr(mixin_view, "permissions_create"):
                permissions = mixin_view.permissions_create

        class UV(UpdateView, mixin_view):
            if (hasattr(mixin_view, "permissions") and
                    mixin_view.permissions == "default"):
                mixin_view.permissions_change = (
                    mixin_view.model._meta.app_label + ".change_" +
                    mixin_view.model._meta.model_name)
            if hasattr(mixin_view, "permissions_change"):
                permissions = mixin_view.permissions_change

        class DV(DeleteView, mixin_view):
            if (hasattr(mixin_view, "permissions") and
                    mixin_view.permissions == "default"):
                mixin_view.permissions_delete = (
                    mixin_view.model._meta.app_label + ".delete_" +
                    mixin_view.model._meta.model_name)
            if hasattr(mixin_view, "permissions_delete"):
                permissions = mixin_view.permissions_delete

        if not hasattr(cls, "create_view") or cls.create_view is None:
            cls.create_view = CV
        if not hasattr(cls, "update_view") or cls.update_view is None:
            cls.update_view = UV
        if not hasattr(cls, "delete_view") or cls.delete_view is None:
            cls.delete_view = DV

        if isinstance(cls.create_view, type):
            urls.append(
                url(r'', cls.create_view.as_view(**initkwargs)))
        if isinstance(cls.update_view, type):
            urls.append(
                url(r'', cls.update_view.as_view(**initkwargs)))
        if isinstance(cls.delete_view, type):
            urls.append(
                url(r'', cls.delete_view.as_view(**initkwargs)))

        if hasattr(cls, "menu_place") and hasattr(cls, "get_menu"):
            cls.menu_place.append_child(cls.get_menu, cls.module_name)

        return include(urls, namespace=namespace)

    @classonlymethod
    def as_view(cls, **initkwargs):
        namespace = None
        if 'namespace' in initkwargs:
            namespace = initkwargs["namespace"]
            del initkwargs["namespace"]

        url_list = url(r'^list/$',
                       super(ListView, cls).as_view(**initkwargs),
                       name=cls.view_name)
        urls = [
            url_list,
        ]

        if (hasattr(cls, "create_view") and
                cls.create_view is not None):
            if isinstance(cls.create_view, type):
                urls.append(
                    url(r'', cls.create_view.as_view(**initkwargs)))
            else:
                if "url_template" not in cls.create_view:
                    cls.create_view["url_template"] = cls._default_url_templates["create"]

        if (hasattr(cls, "update_view") and
                cls.update_view is not None):
            if isinstance(cls.update_view, type):
                urls.append(
                    url(r'', cls.update_view.as_view(**initkwargs)))
            else:
                if "url_template" not in cls.update_view:
                    cls.update_view["url_template"] = cls._default_url_templates["update"]
        
        if (hasattr(cls, "delete_view") and
                cls.delete_view is not None):
            if isinstance(cls.delete_view, type):
                urls.append(
                    url(r'', cls.delete_view.as_view(**initkwargs)))
            else:
                if "url_template" not in cls.delete_view:
                    cls.delete_view["url_template"] = cls._default_url_templates["delete"]

        if cls.module_name and hasattr(cls, "get_menu"):
            cls.menu_place.append_child(cls.get_menu, cls.module_name)

        return include(urls)

    def _page_title(self):
        if hasattr(self, "page_title"):
            return self.page_title
        else:
            vn = self.objects_list.model._meta.verbose_name_plural
            return vn.capitalize()

    def _page_subtitle(self):
        if hasattr(self, "page_subtitle"):
            return self.page_subtitle
        else:
            return "lista"

    def getKeepUrl(self):
        return urlencode(self.keep_vars)

    def keep_var(self, var_name, value):
        if value is not None:
            self.keep_vars[var_name] = value

    def configOrderBy(self, request):
        order_by = list()
        if 'order' in request.GET and request.GET.get('order') != "":
            orders = request.GET.get('order')
            orders = orders.split("|")
            for order in orders:
                if len(order) > 0:
                    if (order[0] == "-" or order[0] == "+"):
                        order_type = order[0]
                        order = order[1:]
                    else:
                        order_type = ""
                    if order_type == "-":
                        self.fields_info.all[order].order = ""
                    else:
                        self.fields_info.all[order].order = "-"
                    order_by.append(order_type + (order))

        self.order_by = "|".join(order_by)
        return self.order_by

    def configPage(self, request):
        # Configuro pagina

        if 'page' in request.GET:
            page = request.GET.get('page')
            try:
                self.page = int(page)
            except ValueError:
                raise Http404
        else:
            self.page = 1

        # configuro page size
        if 'page_size' in request.GET:
            page_size = request.GET.get('page_size')
            try:
                self.page_size = int(page_size)
            except ValueError:
                raise Http404
            request.session['page_size'] = self.page_size
        elif 'page_size' in request.session:
            page_size = request.session['page_size']
            # aqui me gustaria que el valor lo tomara en un view
            try:
                self.page_size = int(page_size)
            except ValueError:
                raise Http404
            finally:
                request.session['page_size'] = self.page_size

    def configSearchCriteria(self, request, search_fields):

        if 'search' in request.GET:
            search = request.GET.get('search')
            try:
                self.search = (search)
            except ValueError:
                raise Http404
        else:
            self.search = None
        ands = None
        if self.search is not None:
            words = self.search.split(" ")
            for word in words:
                ors = None
                for field in search_fields:
                    if ors is None:
                        ors = field.get_search_filter(word.upper())
                    else:
                        ors = ors | field.get_search_filter(word.upper())
                if ands is None:
                    ands = ors
                else:
                    ands = ands & ors

        if ands is not None:
            if self.filter is None:
                self.filter = ands
            else:
                self.filter = self.filter & ands

    def configFilter(self, request):
        if 'id' in request.GET:
            self.id_list = request.GET.getlist('id')
        else:
            self.id_list = None

        if self.id_list is not None:
            if self.filter is None:
                self.filter = Q(pk__in=self.id_list)
            else:
                self.filter = self.filter | Q(pk__in=self.id_list)

    def configFilterFields(self, request):
        ands = None
        for field in self.fields_info.filter_fields:
            var_name = 'filter_' + field.name
            filter_list = list()
            if var_name in request.GET:
                filters = request.GET.getlist(var_name)
                ors = None
                for filter in filters:
                    for f in filter.split("|"):
                        if f != "":
                            if ors is None:
                                ors = Q(**{field.name: f})
                            else:
                                ors = ors | Q(**{field.name: f})
                        else:
                            if ors is None:
                                ors = Q(**{field.name + "__isnull": True})
                            else:
                                ors = (ors |
                                       Q(**{field.name + "__isnull": True}))
                        filter_list.append(f)
                if ands is None:
                    ands = ors
                else:
                    ands = ands & ors
                self.keep_var(var_name, "|".join(filter_list))
                field.propertys["filter_values"] = filter_list
                for f in filter_list:
                    if self.add_filters == "":
                        self.add_filters += "?" + field.name + "=" + f
                    else:
                        self.add_filters += "&" + field.name + "=" + f

        self.filter = ands
        return self.filter

    def configPaginator(self, request):
        self.configPage(request)

        # configuro el paginator
        self.paginator = Paginator(self.objects_list, self.page_size)
        self.paginator.page_sizes = self.page_sizes

    def configureFilterChoices(self):
        for field in self.fields_info.filter_fields:
            if field.hasChoices:
                field.choices = field.get_choices()
            else:
                field.choices = list()
                lista = self.objects_list.order_by(
                    field.name).values(field.name).distinct()
                for item in lista:
                    field.choices.append(
                        (item[field.name], item[field.name]), )

    def configPermissions(self, request):
        def check_perms(perms):
            '''si es lista o string'''
            if isinstance(perms, str):
                return request.user.has_perm(perms)
            else:
                return request.user.has_perms(perms)

        self.perms = {"list": True,
                      "create": True,
                      "update": True,
                      "delete": True}
        if hasattr(self, "permissions"):
            self.perms["list"] = check_perms(self.permissions)
        if hasattr(self, "create_view"):
            if hasattr(self.create_view, "permissions"):
                self.perms["create"] = check_perms(
                    self.create_view.permissions)
        else:
            self.perms["create"] = False

        if hasattr(self, "update_view"):
            if hasattr(self.update_view, "permissions"):
                self.perms["update"] = check_perms(
                    self.update_view.permissions)
        else:
            self.perms["update"] = False

        if hasattr(self, "delete_view"):
            if hasattr(self.delete_view, "permissions"):
                self.perms["delete"] = check_perms(
                    self.delete_view.permissions)
        else:
            self.perms["delete"] = False
        return self.perms

    def pre_process_queryset(self, queryset, request, *args, **kwargs):
        if hasattr(self, "params"):
            filtro = Q()
            for p in self.params:
                arg = kwargs[p]
                filtro = filtro & Q(**{p: arg})
            return queryset.filter(filtro)
        else:
            return queryset

    def get(self, request, *args, **kwargs):
        
        self.objects_list = self.pre_process_queryset(
            self.objects_list, request, *args, **kwargs)
        self.export = request.GET.get("export", False)
        self.fields_info = FieldsInfo(self.objects_list)
        self.fields_info.url_templates = self.url_templates
        # registos totales
        self.original_count = self.objects_list.count()
        # configuro list_display
        if self.list_display is None:
            self.list_display = list()
            for item in self.fields_info.model._meta.fields:
                self.list_display.append(item.name)
        self.fields_info.setListDisplay(self.list_display)

        # configuro fields_properys
        if self.fields_propertys is not None:
            self.fields_info.setFieldPropertys(self.fields_propertys)

        # configuro search_fields
        if self.search_fields is not None:
            has_search = True
            self.fields_info.setSearchFields(self.search_fields)
        else:
            has_search = False

        # configuro filter_fields
        if self.filter_fields is not None:
            self.fields_info.setFilterFields(self.filter_fields)
            self.configureFilterChoices()
            filters = self.configFilterFields(request)
            if filters is not None:
                self.objects_list.filter(filters)

        # configuro search
        self.configSearchCriteria(request, self.fields_info.search_fields)
        self.configFilter(request)
        if self.filter is not None:
            self.objects_list = self.objects_list.filter(self.filter)

        # configuro order_by
        self.configOrderBy(request)
        if self.order_by is not None and self.order_by != "":
            self.objects_list = self.objects_list.order_by(
                *tuple(self.order_by.split("|")))

        self.objects_list = self.objects_list.distinct()

        # registros a mostrar
        self.show_count = self.objects_list.count()
        self.configPaginator(request)

        # si el numero de pagina se exede ir al final
        if self.page > self.paginator.num_pages:
            self.page = self.paginator.num_pages
        self.objects_page = self.paginator.page(self.page)

        # la variable page hay que mantenerla ante cambios en los filtros, etc
        self.keep_var('page', self.page)
        self.keep_var('search', self.search)
        self.keep_var('order', self.order_by)
        self.keep_var('order_type', self.order_type)

        # configuramos los permisos para los links add edit delete
        self.configPermissions(request)

        # print self.fields_info.list_display[0].verbose_name
        breadcrumbs = self.get_breadcrumbs(request)
        if breadcrumbs:
            breadcrumb_active = breadcrumbs[-1]
        else:
            breadcrumb_active = None

        if not hasattr(self, "get_inline_menu"):
            self.get_inline_menu = None
        # cargo el template y el contexto
        if not self.export:
            template = self.template_name
            context = {
                'page_title': self._page_title(),
                'page_subtitle': self._page_subtitle(),
                'objects_list': self.objects_list,
                'objects_page': self.objects_page,
                'fields_info': self.fields_info,
                'paginator': self.paginator,
                'keep_url': self.keep_vars,
                'search': self.search,
                'original_count': self.original_count,
                'show_count': self.show_count,
                'filter_count': self.original_count - self.show_count,
                'table_list_template_name': self.table_list_template_name,
                'has_search': has_search,
                'breadcrums': breadcrumbs,
                'breadcrum_active': breadcrumb_active,
                # 'objects_list':self.objects_list,
                'user': request.user,
                'add_filters': self.add_filters,
                'permissions': self.perms,
                'list_view': self,
                'kwargs': kwargs,
            }
            if hasattr(self, "create_view"):
                context['create_view'] = self.create_view
            if hasattr(self, "update_view"):
                context['update_view'] = self.update_view
            if hasattr(self, "delete_view"):
                context['delete_view'] = self.delete_view
        elif self.export == "ods":
            template = self.template_ods_name
            context = {
                'page_title': self._page_title(),
                'page_subtitle': self._page_subtitle(),
                'objects_list': self.objects_list,
                'objects_page': self.objects_page,
                'fields_info': self.fields_info,
                'original_count': self.original_count,
                'show_count': self.show_count,
                'filter_count': self.original_count - self.show_count,
            }

            context = self.get_context_data(request=request, context=context)
            # uncomment for debug
            # response = HttpResponse(template.render(context))
            response = HttpResponse(
                content_type='application/vnd.ms-excel; charset=utf-8',
                content=loader.get_template(template).render(context))
            response['Content-Disposition'] = (
                'attachment; filename="' + self._page_title() + '.xls"')
            return response

        elif self.export == "print":
            template = self.template_print_name
            context = {
                'page_title': self._page_title(),
                'page_subtitle': self._page_subtitle(),
                'objects_list': self.objects_list,
                'objects_page': self.objects_page,
                'fields_info': self.fields_info,
                'original_count': self.original_count,
                'show_count': self.show_count,
                'filter_count': self.original_count - self.show_count,
                'table_list_template_name': self.table_list_template_name,
                'keep_url': self.keep_vars,
            }
            context = self.get_context_data(request=request, context=context)
            response = render(request, template, context)
            return response

        context = self.get_context_data(request=request, context=context)
        # por ultimo llamo a el post_process
        self.postprocess_get(request, context)

        return render(request, template, context)

    def postprocess_get(self, request, context):
        pass

    def get_context_data(self, request, context):
        return context

    def dispatch(self, request, *args, **kwargs):
        if hasattr(self, "login_required") and self.login_required is True:
            if not request.user.is_authenticated:
                return redirect(
                    "%s?next=%s" % (reverse('accounts:login'), request.path))
        if hasattr(self, "permissions"):
            if isinstance(self.permissions, types.StringTypes):
                if self.permissions == "login":
                    if not request.user.is_authenticated:
                        return HttpResponseForbidden()
                elif not request.user.has_perm(self.permissions):
                    return HttpResponseForbidden()
            else:
                if not request.user.has_perms(self.permissions):
                    return HttpResponseForbidden()

        return super(ListView, self).dispatch(request, *args, **kwargs)

    def get_breadcrumbs(self, request):
        breadcrums = list()
        if hasattr(self, "module_name"):
            module = MenuManager.left_menu.search_by_key(self.module_name)
            if module:
                breadcrums.append(Breadcrumb(module.label))
            breadcrums.append(Breadcrumb(self._page_title))
            return breadcrums
        else:
            return None


def handler403(request):
    response = render(request, 'generic_views/403.html', {})
    response.status_code = 403
    return response


def handler500(request):
    response = render(request, 'generic_views/500.html', {})
    response.status_code = 500
    return response


def error_modal(title, msg, status_code=200):
    context = {
        'title': title,
        'msg': msg,
    }
    response = render(None, 'generic_views/error_modal.html', context)
    response.status_code = status_code
    return response


class Breadcrumb():
    url = None
    active = False

    def __init__(self, label):
        self.label = label
